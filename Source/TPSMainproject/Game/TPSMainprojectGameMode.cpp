// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSMainprojectGameMode.h"
#include "TPSMainprojectPlayerController.h"
#include "TPSMainproject/Character/TPSMainprojectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPSMainprojectGameMode::ATPSMainprojectGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPSMainprojectPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}