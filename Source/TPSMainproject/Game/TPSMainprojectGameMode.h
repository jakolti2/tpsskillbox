// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPSMainprojectGameMode.generated.h"

UCLASS(minimalapi)
class ATPSMainprojectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPSMainprojectGameMode();
};



