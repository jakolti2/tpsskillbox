// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSMainproject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPSMainproject, "TPSMainproject" );

DEFINE_LOG_CATEGORY(LogTPSMainproject)
 